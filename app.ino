#include <stdbool.h>
_Bool vibrationState = 0;
_Bool heatingState = 0;
unsigned char vibrationLevel = 255;
unsigned char heatingLevel = 255;
unsigned char received;
unsigned char received2[1];

#define MOTOR_PIN 11
#define MOTOR2_PIN 10
#define HEATER_PIN 3
#define DEBUG_MODE 0

void setup() {
	pinMode(MOTOR_PIN, OUTPUT);
	Serial.begin(9600);
	Serial.setTimeout(2000);
	Serial.println("MMMY Started");
}
void loop() {
	if (Serial.available() > 0) {
		received = Serial.read();
		//Serial.print("Received from serial: ");
		//Serial.println(received);
		switch (received) {
			case 'a': // status check-in
				Serial.print(vibrationState);
				Serial.write(vibrationLevel);
				Serial.print(heatingState);
				Serial.write(heatingLevel);
				Serial.write(123);
				Serial.println();
				break;
			case 'b': // start vibration
				Serial.println('k');
				vibrationState = 1;
				analogWrite(MOTOR_PIN, vibrationLevel);
				analogWrite(MOTOR2_PIN, vibrationLevel);
				break;
			case 'e': // start heat
				Serial.println('k');
				heatingState = 1;
				analogWrite(HEATER_PIN, heatingLevel);
				break;
			case 'c': // stop vibration
				Serial.println('k');
				vibrationState = 0;
				analogWrite(MOTOR_PIN, LOW);
				analogWrite(MOTOR2_PIN, LOW);
				break;
			case 'f': // stop heat
				Serial.println('k');
				heatingState = 0;
				analogWrite(HEATER_PIN, LOW);
				break;
			case 'd': // change vibration level
				if (Serial.readBytes(received2, 1) == 0) {
					Serial.println('z');
					break;
				}
				vibrationLevel = received2[0];
				Serial.println('k');
				if (DEBUG_MODE == true) {
					Serial.print("Received vibration level: ");
					Serial.println(vibrationLevel);
				}
				analogWrite(MOTOR_PIN, vibrationLevel);
				break;
			case 'g': // change heat level
				if (Serial.readBytes(received2, 1) == 0) {
					Serial.println('z');
					break;
				}
				heatingLevel = received2[0];
				Serial.println('k');
				if (DEBUG_MODE == true) {
					Serial.print("Received heating level: ");
					Serial.println(heatingLevel);
				}
				analogWrite(HEATER_PIN, heatingLevel);
				break;
			default:
				Serial.println('z');
		}
	}
}
